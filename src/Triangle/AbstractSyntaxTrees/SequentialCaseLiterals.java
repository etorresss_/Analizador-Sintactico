/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class SequentialCaseLiterals extends Expression {

  public SequentialCaseLiterals (Expression e1AST, Expression e2AST, SourcePosition thePosition) {
    super (thePosition);
    E1 = e1AST;
    E2 = e2AST;
  }

  public Object visit(Visitor v, Object o) {
    return v.visitSequentialCaseLiterals(this, o);
  }

  public Expression E1, E2;
}
