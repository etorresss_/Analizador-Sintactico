/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class SequentialCaseCommand extends Command {
    
    public SequentialCaseCommand(CaseCommand cc1AST, CaseCommand cc2AST, SourcePosition thePosition) {
        super(thePosition);
        this.CC1 = cc1AST;
        this.CC2 = cc2AST;
    }

    @Override
    public Object visit(Visitor v, Object o) {
        return v.visitSequentialCaseCommand(this, o);
    }
    
    public CaseCommand CC1, CC2;
}

