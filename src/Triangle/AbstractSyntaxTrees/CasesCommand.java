/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class CasesCommand extends Command {

    public CasesCommand(CaseCommand csAST, Command cAST, SourcePosition thePosition) {
        super(thePosition);
        this.CS = csAST;
        this.C = cAST;
    }

    @Override
    public Object visit(Visitor v, Object o) {
        return v.visitCasesCommand(this, o);
    }
    
    public CaseCommand CS;
    public Command C;
}
