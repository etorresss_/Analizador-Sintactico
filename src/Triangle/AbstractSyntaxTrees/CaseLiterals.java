/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class CaseLiterals extends Expression {

    public CaseLiterals(Expression e1AST, SourcePosition thePosition) {
        super(thePosition);
        E = e1AST;
    }
    
    public Object visit(Visitor v, Object o) {
        return v.visitCaseLiterals(this, o);
    }
    
    public Expression E;
    
}
