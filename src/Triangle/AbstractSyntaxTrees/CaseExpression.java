/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class CaseExpression extends Expression {

    public CaseExpression(Expression e1AST, Expression e2AST, SourcePosition thePosition) {
        super(thePosition);
        this.E1 = e1AST;
        this.E2 = e2AST;
    }

    public Object visit(Visitor v, Object o) {
        return v.visitCaseExpression(this, o);
    }
    
    public Expression E1, E2;
}
