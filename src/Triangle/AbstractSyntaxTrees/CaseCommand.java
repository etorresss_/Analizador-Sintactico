/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triangle.AbstractSyntaxTrees;

import Triangle.SyntacticAnalyzer.SourcePosition;

/**
 *
 * @author Eduard
 */
public class CaseCommand extends Command {

    public CaseCommand(CaseLiterals CL, Command C, SourcePosition thePosition) {
        super(thePosition);
        this.CL = CL;
        this.C = C;
    }

    public Object visit(Visitor v, Object o) {
        return v.visitCaseCommand(this, o);
    }
    
    public CaseLiterals CL;
    public Command C;
}
